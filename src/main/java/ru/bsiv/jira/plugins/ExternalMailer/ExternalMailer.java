package ru.bsiv.jira.plugins.ExternalMailer;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.util.TextUtil;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.pico.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.mail.IssueTemplateContext;
import com.atlassian.jira.mail.JiraMailQueueUtils;
import com.atlassian.jira.mail.TemplateContextFactory;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.TemplateManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.util.TextUtils;


public class ExternalMailer implements InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(ExternalMailer.class);
    private final EventPublisher eventPublisher;
    private NotificationSchemeManager notificationSchemeManager;

    public ExternalMailer(EventPublisher eventPublisher) {
        super();
        this.eventPublisher = eventPublisher;
    }

    public void afterPropertiesSet() throws Exception {
        this.eventPublisher.register(this);
    }

    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }

    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
        Long eventTypeId = issueEvent.getEventTypeId();
        log.warn("ExternalMailer: Event fired. ID = " + eventTypeId + " " + issueEvent);
        Issue issue = issueEvent.getIssue();
        CustomFieldManager cfManager = ComponentAccessor.getCustomFieldManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        if (eventTypeId.equals(EventType.ISSUE_CREATED_ID)) {
            log.warn("ExternalMailer:Issue has been created " + issue.getKey());
            MutableIssue changeItemList = issueManager.getIssueObject(issue.getKey());
            CustomField changeItemListIterator = cfManager.getCustomFieldObjectByName("Due Date Counter");
            DefaultIssueChangeHolder changeItem = new DefaultIssueChangeHolder();
            changeItemListIterator.updateValue((FieldLayoutItem) null, changeItemList, new ModifiedValue(changeItemList.getCustomFieldValue(changeItemListIterator), Double.valueOf(Double.parseDouble("0.0"))), changeItem);
            CustomField fieldName = cfManager.getCustomFieldObjectByName("Initial Due Date");
            Timestamp oldValue = issue.getDueDate();
            if (oldValue != null) {
                fieldName.updateValue((FieldLayoutItem) null, changeItemList, new ModifiedValue(changeItemList.getCustomFieldValue(changeItemListIterator), oldValue), changeItem);
            }

            this.sendNotification(issueEvent);
        } else if (eventTypeId.equals(EventType.ISSUE_RESOLVED_ID)) {
            log.warn("ExternalMailer:Issue has been resolved " + issue.getKey());
            this.sendNotification(issueEvent);
        } else if (eventTypeId.equals(EventType.ISSUE_COMMENTED_ID)) {
            log.warn("ExternalMailer:Issue has been commented " + issue.getKey());
            this.sendNotification(issueEvent);
        } else if (eventTypeId.equals(EventType.ISSUE_ASSIGNED_ID)) {
            log.warn("ExternalMailer:Issue has been assigned " + issue.getKey());
            this.sendNotification(issueEvent);
        } else if (eventTypeId.equals(EventType.ISSUE_UPDATED_ID) || eventTypeId.longValue() == 13L) {
            log.warn("ExternalMailer:Issue has been changed " + issue.getKey());
            List changeItemList1 = null;

            try {
                changeItemList1 = issueEvent.getChangeLog().getRelated("ChildChangeItem");
                Map changeItemListIterator1 = issueEvent.getParams();
                Object changeItem1 = changeItemListIterator1.get("eventsource");
                log.warn("ExternalMailer:eventsource= " + changeItem1);
            } catch (GenericEntityException var20) {
                var20.printStackTrace();
            }

            this.sendNotification(issueEvent);
            if (changeItemList1 == null) {
                return;
            }
        }
    }

    protected void sendNotification(IssueEvent event) {
        if (event.isSendMail()) {
            log.warn("ExternalMailer:Send notification...");
            GenericValue projectGV = event.getIssue().getProjectObject().getGenericValue();
            if (this == null) {
                log.error("ExternalMailer instance is null!!!");
            }

            if (ComponentAccessor.getNotificationSchemeManager() == null) {
                log.error("ComponentAccessor.getNotificationSchemeManager() is null!!!");
            }

            this.notificationSchemeManager = ComponentAccessor.getNotificationSchemeManager();
            GenericValue notificationScheme = this.notificationSchemeManager.getNotificationSchemeForProject(projectGV);
            if (notificationScheme != null) {
                try {
                    this.createMailItems(notificationScheme, event);
                } catch (PermissionException var5) {
                    log.error("Permission error:" + var5.getMessage());
                }
            }
        }

    }

    protected void createMailItems(GenericValue notificationScheme, IssueEvent event) throws
            PermissionException {
        try {
            Date e = new Date();
            log.warn(e.toString() + ":ExternalMailer:Create mail items...");
            List notificationSchemeEntities = this.notificationSchemeManager.getEntities(notificationScheme, event.getEventTypeId());
            log.warn("Entity count = " + notificationSchemeEntities.size());
            HashSet allRecipients = new HashSet();
            Iterator iterator = notificationSchemeEntities.iterator();

            while (iterator.hasNext()) {
                GenericValue schemeEntity = (GenericValue) iterator.next();
                log.warn(String.format("---> schemeid %s \n type %s \n eventTypeId %s \n templateId %s \n"
                        , schemeEntity.getLong("id")
                        , schemeEntity.getString("type")
                        , schemeEntity.get("eventTypeId")
                        , schemeEntity.get("templateId")));
                SchemeEntity notificationSchemeEntity = new SchemeEntity(schemeEntity.getLong("id"), schemeEntity.getString("type"), schemeEntity.getString("parameter"), schemeEntity.get("eventTypeId"), schemeEntity.get("templateId"), schemeEntity.getLong("scheme"));
                TemplateManager tmplmgr = ComponentAccessor.getOSGiComponentInstanceOfType(TemplateManager.class);
                Long templateId = tmplmgr.getTemplate(notificationSchemeEntity).getId();
                log.warn("templateID =" + templateId);
                Set intendedRecipients = this.notificationSchemeManager.getRecipients(event, notificationSchemeEntity);
                log.warn("Recepients count = " + intendedRecipients.size());
                HashSet actualRecipients = new HashSet();
                UserUtil userUtil = ComponentAccessor.getUserUtil();
                if (intendedRecipients != null && !intendedRecipients.isEmpty()) {
                    int c = this.getMaxDummyIndex();
                    if (c > 1) {
                        int iterator1;
                        for (iterator1 = 0; userUtil.userExists("dummy" + iterator1); ++iterator1) {
                            userUtil.removeUser(userUtil.getUser("jira_admin"), userUtil.getUser("dummy" + iterator1));
                        }

                        c = 0;
                        log.warn("Deleted " + iterator1 + " users...");
                    }

                    Iterator var31 = intendedRecipients.iterator();

                    while (var31.hasNext()) {
                        String index = (new Integer(c++)).toString();
                        NotificationRecipient notificationRecipient = (NotificationRecipient) var31.next();
                        if (!allRecipients.contains(notificationRecipient.getUser().getDirectoryUser().getName())) {
                            ApplicationUser user = notificationRecipient.getUser();
                            log.warn("Recipient name = " + user.getName());
                            UserPropertyManager userPropertyManager = ComponentAccessor.getUserPropertyManager();
                            PropertySet ps = userPropertyManager.getPropertySet(user);
                            String email = ps.getString("jira.meta.email");
                            log.warn("Recipient Email = " + email);
                            if (email != null && email != "") {
                                ApplicationUser dummy = userUtil.getUserByName("jira_admin");
                                NotificationRecipient nrNew = new NotificationRecipient(dummy);
                                log.warn("dummy user email = " + nrNew.getEmail() + " format = " + nrNew.getFormat());
                                allRecipients.add(notificationRecipient.getUser().getName());
                                Map contextParams = this.getIssueContextParams(event);
                                String templateContent = ComponentAccessor.getOSGiComponentInstanceOfType(TemplateManager.class).getTemplateContent(templateId, "text");
                                log.warn("templateContent =" + templateContent);
                                String body = ComponentAccessor.getVelocityManager().getEncodedBodyForContent(templateContent, (String) event.getParams().get("baseurl"), contextParams);
                                String newBody = ComponentAccessor.getVelocityManager().getEncodedBodyForContent(templateContent, null, contextParams);

                                log.warn("body is now:" + body);
                                log.warn("newBody is now:" + newBody);

                                Email mail = new Email(email);
                                mail.setSubject(this.getSubject(templateId, event));
                                mail.setMimeType("text/plain");
                                mail.setBody(body);
                                SingleMailQueueItem mitem = new SingleMailQueueItem(mail);
                                ComponentAccessor.getMailQueue().addItem(mitem);
                            }
                        } else {
                            log.warn("Multiple event (" + event.getEventTypeId() + ") notification emails intended for the recipient: " + notificationRecipient.getUser() + ". Sending first intended email only for the event.");
                        }
                    }

                    if (!actualRecipients.isEmpty()) {
                        ;
                    }
                }
            }
        } catch (GenericEntityException var30) {
            log.error("There was an error accessing the notifications for the scheme: " + notificationScheme.getString("name") + "." + var30.getMessage());
        }

    }

    private int getMaxDummyIndex() {
        int ret = 0;

        for (UserUtil userUtil = ComponentAccessor.getUserUtil(); userUtil.userExists("dummy" + ret); ++ret) {
            ;
        }

        return ret;
    }

    public String getSubject(Long templateId, IssueEvent event) {
        JiraAuthenticationContext jiraAuthenticationContext = (JiraAuthenticationContext) ComponentManager.getInstance().getComponentInstanceOfType(JiraAuthenticationContext.class);
        I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();

        try {
            String e = ComponentAccessor.getOSGiComponentInstanceOfType(TemplateManager.class).getTemplateContent(templateId, "subject");
            Map contextParams = this.getIssueContextParams(event);
            contextParams.put("i18n", i18n);
            contextParams.put("eventTypeName", ((IssueTemplateContext) contextParams.get("context")).getEventTypeName(i18n));
            OutlookDate formatter = new OutlookDate(i18n.getLocale());
            contextParams.put("dateformatter", formatter);
            String subj = ComponentAccessor.getVelocityManager().getEncodedBodyForContent(e, (String) event.getParams().get("baseurl"), contextParams);
            log.warn("Subject=" + subj);
            return subj;
        } catch (Exception var9) {
            log.error("Could not determine subject", var9.getMessage());
            return i18n.getText("bulk.bean.initialise.error");
        }
    }

    protected Map getIssueContextParams(IssueEvent iEvent) throws GenericEntityException {
        HashMap contextParams = new HashMap();
        JiraAuthenticationContext jiraAuthenticationContext = (JiraAuthenticationContext) ComponentManager.getInstance().getComponentInstanceOfType(JiraAuthenticationContext.class);
        I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
        TemplateContextFactory templateContextFactory = null;
        List componentsOfType = ComponentManager.getInstance().getComponentsOfType(TemplateContextFactory.class);

        TemplateContextFactory templateContextFactory1 = null;
        for (Iterator templateContext = componentsOfType.iterator(); templateContext.hasNext(); templateContextFactory = templateContextFactory1) {
            templateContextFactory1 = (TemplateContextFactory) templateContext.next();
        }

        if (templateContextFactory1 != null) {
            log.warn("getting templateContext1");
            IssueTemplateContext templateContext1 = (IssueTemplateContext) templateContextFactory.getTemplateContext(i18n.getLocale(), iEvent);
            if (templateContext1 == null) {
                log.error("templateContext1 is null");
            } else {
                contextParams.put("comment", templateContext1.getCommentParams());
                contextParams.putAll(templateContext1.getTemplateParams());

            }
        } else {
            log.error("templateContextFactory1 is null");
        }

        log.warn("Ctx Params=" + JiraMailQueueUtils.getContextParamsBody(contextParams));
        return JiraMailQueueUtils.getContextParamsBody(contextParams);

    }


}


